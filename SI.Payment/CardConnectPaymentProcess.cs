﻿using Newtonsoft.Json.Linq;
using SI.Utilities.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace SI.Payment
{
    public class CardConnectPaymentProcess
    {
        public PaymentResponse Capture(PaymentRequest PaymentRequest)
        {
            //PaymentGatewayCredential authRequest

            PaymentResponse res = new PaymentResponse();
            try
            {

                CardConnectRestClient client = new CardConnectRestClient(PaymentRequest.paymentGatewayCredential.EndPointURL, PaymentRequest.paymentGatewayCredential.UserName, PaymentRequest.paymentGatewayCredential.Password);
                JObject authReq = new JObject();
                authReq.Add("name", PaymentRequest.FirstName + " " + PaymentRequest.LastName);
                authReq.Add("address", PaymentRequest.Address1 + " " + PaymentRequest.Addres2);
                authReq.Add("merchid", PaymentRequest.paymentGatewayCredential.MerchantID);
                authReq.Add("account", PaymentRequest.Account);
                authReq.Add("amount", PaymentRequest.PaymentDetail.Amount);
                authReq.Add("expiry", PaymentRequest.ExpiryDate);
                authReq.Add("cvv2", PaymentRequest.CVV);
                authReq.Add("currency", "USD");

                JObject authRes = client.authorizeTransaction(authReq);

                if (authRes != null)
                {
                    string ResponseText = Convert.ToString(authRes.GetValue("resptext"));
                    if (ResponseText == "Approval")
                    {
                        JObject request = new JObject();
                        request.Add("merchid", PaymentRequest.paymentGatewayCredential.MerchantID);
                        request.Add("retref", (String)authRes.GetValue("retref"));

                        JObject captureReq = client.captureTransaction(request);

                        res.Amount = (String)captureReq.GetValue("amount");
                        res.ResponseText = (String)captureReq.GetValue("resptext");
                        res.BatchID = (String)captureReq.GetValue("batchid");
                        res.ReturnRef = (String)captureReq.GetValue("retref");
                        res.ResponseCode = (String)captureReq.GetValue("respcode");
                        res.Account = (String)captureReq.GetValue("account");
                        res.DefaultAccount = (String)captureReq.GetValue("defaultacct");
                        res.MerchentID = (String)captureReq.GetValue("merchid");
                        res.Token = (String)captureReq.GetValue("token");
                    }
                    else
                    {
                        res.Amount = (String)authRes.GetValue("amount");
                        res.ResponseText = (String)authRes.GetValue("resptext");
                        res.BatchID = (String)authRes.GetValue("batchid");
                        res.ReturnRef = (String)authRes.GetValue("retref");
                        res.ResponseCode = (String)authRes.GetValue("respcode");
                        res.Account = (String)authRes.GetValue("account");
                        res.DefaultAccount = (String)authRes.GetValue("defaultacct");
                        res.MerchentID = (String)authRes.GetValue("merchid");
                        res.Token = (String)authRes.GetValue("token");
                    }

                }
            }
            catch (Exception ex) { }
            return res;
        }

    }
}

