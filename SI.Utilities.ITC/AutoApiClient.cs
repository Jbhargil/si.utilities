﻿using System;

namespace SI.Utilities.ITC
{
    public class AutoApiClient
    {
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public Guid AgencyID { get; set; }
        public string IMPAccountID { get; set; }
        public string IMPAgencyID { get; set; }
        public bool IsTestMode { get; set; }
        public string RaterAPIUrl { get; set; }
        public string Type { get; set; } = "All";
        public bool IncludeCompanyQuestions { get; set; } = true;
        public bool IsReturnAll { get; set; } = true;
    }
}
