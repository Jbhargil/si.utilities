﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Microsoft.OData.Client;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace SI.Utilities.ITC
{
    public class AutoVehicleAPI
    {
        public List<Years> GetVehicleYears(string url, AutoApiClient credential)
        {
            string impAccountId = credential.IMPAccountID;
            List<Years> lstYears = new List<Years>();
            string response = GetResponse(url, impAccountId);
            lstYears = JsonConvert.DeserializeObject<List<Years>>(response);
            return lstYears;
        }

        private string GetResponse(string url, string impAccountId)
        {
            string response = string.Empty;
            using (var client = new WebClient())
            {
                client.Headers.Add("content-type", "application/json");
                client.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(impAccountId.ToString() + ":" + impAccountId.ToString())));
                response = client.DownloadString(url);
            }
            return response;
        }

        public List<VehicleList> GetVehicleList(string url, AutoApiClient credential)
        {
            string impAccountId = credential.IMPAccountID;
            List<VehicleList> lstVehile = new List<VehicleList>();
            string response = GetResponse(url, impAccountId);
            response = response.Replace("[", " ").Replace("]", " ");
            foreach (string item in response.Split(','))
            {
                VehicleList _veh = new VehicleList();
                _veh.Vehicle = Regex.Replace(item, @"[\""]", "", RegexOptions.None).TrimStart().TrimEnd();
                lstVehile.Add(_veh);
            }
            return lstVehile;
        }

        public List<VehicleModel> GetVehicleModelList(string url, AutoApiClient credential)
        {
            string impAccountId = credential.IMPAccountID;
            List<VehicleModel> lstVehileModel = new List<VehicleModel>();
            string response = GetResponse(url, impAccountId);
            response = response.Replace("[", " ").Replace("]", " ");
            foreach (string item in response.Split(','))
            {
                VehicleModel _veh = new VehicleModel();
                _veh.VehicleModelName = Regex.Replace(item, @"[\""]", "", RegexOptions.None).TrimStart().TrimEnd();
                lstVehileModel.Add(_veh);
            }
            return lstVehileModel;
        }
        public List<VehicleDetailModel> GetVehicleDetailList(string url, AutoApiClient credential)
        {
            string impAccountId = credential.IMPAccountID;
            List<VehicleDetailModel> lstVehileDetail = new List<VehicleDetailModel>();
            string response = GetResponse(url, impAccountId);
            lstVehileDetail = JsonConvert.DeserializeObject<List<VehicleDetailModel>>(response);
            return lstVehileDetail;
        }

        public VehicleDetailModel GetVehicleDetail(string url, AutoApiClient credential)
        {
            string impAccountId = credential.IMPAccountID;
            //List<VehicleDetailModel> lstVehileDetail = new List<VehicleDetailModel>();
            VehicleDetailModel vehicleDetailModel = new VehicleDetailModel();
            string response = GetResponse(url, impAccountId);
            vehicleDetailModel = JsonConvert.DeserializeObject<VehicleDetailModel>(response);
            return vehicleDetailModel;
        }


    }
}
