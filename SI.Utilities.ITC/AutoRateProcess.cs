﻿using Newtonsoft.Json;
using SI.Utilities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using TurboRater;
using TurboRater.ApiClients.RateEngineApi;
using TurboRater.Insurance;
using TurboRater.Insurance.AU;
using TurboRater.Insurance.DataTransformation;

namespace SI.Utilities.ITC
{
    public class AutoRateProcess
    {
        private string _accessKey;
        public AutoRateProcess(string AccessKey)
        {
            _accessKey = AccessKey;
        }

        public AutoResponseModel GetRates(AutoRequestModel model, AutoApiClient credential, long[] itcCompanyIds = null)
        {
            CompanyInfoRequest infoRequest = GetCompanyInfoRequst(model, credential);

            RateEngineApiClient _rateEngineApiClient = new RateEngineApiClient()
            {
                BaseUrl = credential.RaterAPIUrl
            };

            List<CompanyInfo> _itcCompanyList = GetITCCompanyInfo(infoRequest, _rateEngineApiClient);

            if (itcCompanyIds != null)
            {
                _itcCompanyList = _itcCompanyList.Where(w => itcCompanyIds.Count(c => c == w.CompanyID) > 0).ToList();
            }

            ITCRateEngineRequest request = GetITCRateEngineRequest(model, credential, AutoCreatePolicy(model));

            List<ResponseCompanyDetails> companyDetailsfromList = GetCarrerInformation(credential, _itcCompanyList, request);

            var _autoResponseModel = AutoGetRateResponse(_rateEngineApiClient, request, credential, companyDetailsfromList);

            return _autoResponseModel;
        }

        private static CompanyInfoRequest GetCompanyInfoRequst(AutoRequestModel model, AutoApiClient credential) => new CompanyInfoRequest()
        {
            AccountNumber = credential.AccountNumber,
            AccessID = credential.AccountName,
            AccountName = credential.AccountName,
            State = model.AutoPolicy.StateCd,
            AgencyId = credential.AgencyID,
            Type = credential.Type,
            IncludeCompanyQuestions = credential.IncludeCompanyQuestions
        };

        internal ITCRateEngineRequest GetITCRateEngineRequest(AutoRequestModel model, AutoApiClient credential, AUPolicy _policy) => new ITCRateEngineRequest
        {
            AccessID = credential.AccountName,
            AccountName = credential.AccountName,
            AccountNumber = credential.AccountNumber,
            BumpLimits = RateEngineBumpingEnum.bBumpUp,
            RealTimeAccountNumber = credential.AccountName,
            TransactionID = Guid.NewGuid().ToString(),
            CustomerID = Guid.NewGuid().ToString(),
            RateState = model.AutoPolicy.StateCd,
            PolicyData = ExportPolicy(_policy),
            LineOfInsurance = InsuranceLine.PersonalAuto,
            EstimateTerm = true,
            InsuredFirstName = model.AutoPolicy.FirstName,
            InsuredMiddleName = model.AutoPolicy.MiddleName,
            InsuredLastName = model.AutoPolicy.LastName,
            Test = credential.IsTestMode,
            UseRequestCredentials = false
        };

        internal static List<CompanyInfo> GetITCCompanyInfo(CompanyInfoRequest infoRequest, RateEngineApiClient _rateEngineApiClient)
        {
            return _rateEngineApiClient.GetCompanyInfo(infoRequest).CompanyInfoList.Where(f => f.Active == true).ToList();
        }

        private static AutoResponseModel AutoGetRateResponse(RateEngineApiClient _rateEngineApiClient,
            ITCRateEngineRequest request, AutoApiClient credential, List<ResponseCompanyDetails> responseCompanyDetails)
        {
            AutoResponseModel _autoResponseModel = new AutoResponseModel();
            try
            {

                _rateEngineApiClient.RatePolicy(request);
                List<ITCRateEngineResponse> _allITCRateResponses = new List<ITCRateEngineResponse>();
                System.Threading.Thread.Sleep(5000);
                while (_allITCRateResponses.Any(w => w.AllRatingComplete == false) || _allITCRateResponses.Count == 0)
                {
                    _allITCRateResponses = _rateEngineApiClient.GetRateResults(request.TransactionID, credential.IsReturnAll);
                }
                _autoResponseModel.ResponseFile = JsonConvert.SerializeObject(value: _allITCRateResponses);

                CalculateResponse(_autoResponseModel, _allITCRateResponses, responseCompanyDetails);
            }
            catch (Exception ex)
            {
            }
            return _autoResponseModel;
        }

        internal static List<ResponseCompanyDetails> GetCarrerInformation(AutoApiClient credential, List<CompanyInfo> _itcCompanyList, ITCRateEngineRequest request)
        {
            List<ResponseCompanyDetails> _comp = new List<ResponseCompanyDetails>();
            foreach (var cmp in _itcCompanyList)
            {
                ResponseCompanyDetails details = new ResponseCompanyDetails();
                request.CarrierInformation.Add(new CarrierInfo()
                {
                    AgencyID = credential.AgencyID,
                    CarrierID = string.Empty,
                    CarrierPassword = string.Empty,
                    CompanyID = cmp.CompanyID,
                    DOSCompanyID = cmp.DOSCompanyID,
                    OrderCreditScore = cmp.CreditRequired,
                    ProgramID = cmp.ProgramID,
                    ProducerCode = string.Empty,
                    SubProducerCode = string.Empty,
                    CompanyName = cmp.CompanyName
                });
                details.CompanyId = cmp.CompanyID;
                details.CompanyName = cmp.CompanyName;
                _comp.Add(details);
            }
            return _comp;
        }

        internal AUPolicy AutoCreatePolicy(AutoRequestModel model)
        {
            var policy = new AUPolicy(InsuranceLine.PersonalAuto);

            var insured = new AUDriver(TypeOfPerson.NamedInsured, InsuranceLine.PersonalAuto)
            {
                Policy = policy,
                FirstName = model.AutoPolicy.FirstName,
                LastName = model.AutoPolicy.LastName,
                DOB = (DateTime)model.AutoPolicy.BirthDate,
                Address1 = model.AutoPolicy.Street_Address1,
                Address2 = model.AutoPolicy.Street_Address2,
                City = model.AutoPolicy.City,
                State = ITCCommonFunc.GetITCStateValue(model.AutoPolicy.StateCd),
                Relation = ITCConstants.RelationChars[(Convert.ToInt32(Relation.Insured))],
                Sex = ITCConstants.GenderChars[(Convert.ToInt32(Gender.Male))],
                ZipCode = model.AutoPolicy.ZipCode
            };


            policy.Insured = insured;
            policy.EffectiveDate = model.AutoPolicy.EffectiveDate;
            policy.MailingAddress.Address1 = model.AutoPolicy.MailingStreet_Address1;
            policy.MailingAddress.Address2 = model.AutoPolicy.MailingStreet_Address2;
            policy.MailingAddress.City = model.AutoPolicy.MailingCity;
            policy.MailingAddress.State = ITCCommonFunc.GetITCStateValue(model.AutoPolicy.MailingStateCd);
            policy.MailingAddress.ZipCode = model.AutoPolicy.MailingZipCode;
            policy.Term = model.AutoPolicy.PolicyTerm;

            AddAutoDrivers(model, policy);

            AddAutoVehicles(model, policy);

            policy.CoveragesByCar = true;

            return policy;
        }

        internal static void AddAutoDrivers(AutoRequestModel model, AUPolicy policy)
        {
            foreach (var qd in model.AutoDrivers)
            {
                AUDriver driver = new AUDriver
                {
                    FirstName = qd.FirstName,
                    LastName = qd.LastName,
                    Licensed = !string.IsNullOrWhiteSpace(qd.License_Number) ? true : false
                };
                if (!string.IsNullOrWhiteSpace(qd.License_StateCd))
                {
                    driver.LicensedState = true;
                    driver.StateLicensed = qd.License_StateCd;
                }
                else
                {
                    driver.LicensedState = false;
                }
                driver.DrvLicenseNumber = qd.License_Number;
                driver.DOB = Convert.ToDateTime(qd.BirthDate);//DbFunction.ParseDriverDOB(qd.DOB, qd.BirthDate);
                driver.Sex = qd.Gender.StartsWith("M") ? "M" : "F";
                driver.Marital = ITCConstants.MaritalChars.FirstOrDefault(f => f == qd.Marital_Status.Substring(0, 1));
                driver.Relation = qd.RelationCd;
                driver.SR22 = qd.IsSR22;
                driver.GoodCredit = true;
                foreach(var qdViolation in qd.AutoDriverViolations)
                {
                    AUViolation auViolation = new AUViolation();
                    auViolation.CustomViolCode = Convert.ToInt32(qdViolation.VioCode1);
                    auViolation.ViolCode = (!string.IsNullOrEmpty(qdViolation.VioCode2) ? Convert.ToInt32(qdViolation.VioCode2) : 0);
                    auViolation.ViolDate = qdViolation.VioDate;
                    auViolation.MVRAppeal = qdViolation.IsMVRAppeal;
                    auViolation.AtFault = qdViolation.AtFault;
                    auViolation.DriverLinkID = qd.Id;
                    driver.Violation.Add(auViolation);
                }
                policy.Drivers.Add(driver);
            }
        }

        internal static void AddAutoVehicles(AutoRequestModel model, AUPolicy policy)
        {

            foreach (var veh in model.AutoVehicles)
            {
                AUCar car = new AUCar
                {
                    ZipCode = Convert.ToInt32(model.AutoPolicy.ZipCode),
                    VIN = veh.VIN,
                    Maker = veh.Make,
                    Model = veh.Model,
                    Year = Convert.ToInt32(veh.Year)
                };
                
                if (!string.IsNullOrWhiteSpace(model.Coverage.BodilyInjuryPerAcc) && !string.IsNullOrWhiteSpace(model.Coverage.BodilyInjuryPerPerson))
                {
                    car.LiabBI = true;
                    car.LiabLimits1 = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.BodilyInjuryPerPerson));
                    car.LiabLimits2 = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.BodilyInjuryPerAcc));
                }
                else
                {
                    car.LiabBI = false;
                }
                if (!string.IsNullOrWhiteSpace(model.Coverage.PropertyDamage))
                {
                    car.LiabPD = true;
                    car.LiabLimits3 = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.PropertyDamage));
                }
                else
                {
                    car.LiabPD = false;
                }
                if (!string.IsNullOrWhiteSpace(model.Coverage.UninsuredBodilyInjuryPerPerson) && !string.IsNullOrWhiteSpace(model.Coverage.UninsuredBodilyInjuryPerAcc))
                {
                    car.UninsBI = true;
                    car.UninsBILimits1 = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.UninsuredBodilyInjuryPerPerson));
                    car.UninsBILimits2 = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.UninsuredBodilyInjuryPerAcc));
                }
                else
                {
                    car.UninsBI = false;
                }

                if (!string.IsNullOrWhiteSpace(model.Coverage.UninsuredPropertyDamage))
                {
                    car.UninsPD = true;
                    car.UninsPDLimit = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.UninsuredPropertyDamage));
                }
                else
                {
                    car.UninsPD = false;
                }

                if (!string.IsNullOrWhiteSpace(veh.ComprehensiveDed))
                {
                    car.Comp = true;
                    car.CompDed = Convert.ToInt32(veh.ComprehensiveDed);
                }
                else
                {
                    car.Comp = false;
                }
                if (!string.IsNullOrWhiteSpace(veh.CollisionDed))
                {
                    car.Coll = true;
                    car.CollDed = Convert.ToInt32(veh.CollisionDed);
                }
                else
                {
                    car.Coll = false;
                }
                if (!string.IsNullOrWhiteSpace(model.Coverage.MedicalPayments))
                {
                    car.MedPay = true;
                    car.MedPayLimit = Convert.ToInt32(model.Coverage.MedicalPayments);
                }
                else
                {
                    car.MedPay = false;
                }
                if (!string.IsNullOrWhiteSpace(model.Coverage.PersonalInjuryProtection))
                {
                    car.PIP = true;
                    car.PIPLimit = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.PersonalInjuryProtection));
                    car.PIPDed = Convert.ToInt32(model.Coverage.PersonalInjuryProtectionDed);
                }
                else
                {
                    car.PIP = false;
                }
                if (!string.IsNullOrWhiteSpace(model.Coverage.PropertyProtectionInsurance))
                {
                    car.PPI = true;
                    car.PPILimit = ITCCommonFunc.ITCCoverageConverter(Convert.ToInt32(model.Coverage.PropertyProtectionInsurance));
                }
                else
                {
                    car.PPI = false;
                }
                if (veh.CarPurchaseCost > 0)
                {
                    car.PurchaseCost = Convert.ToInt32(veh.CarPurchaseCost);
                    car.MSRP = Convert.ToInt32(veh.CarPurchaseCost);
                }
                policy.Cars.Add(car);
            }
        }

        internal string ExportPolicy(AUPolicy policy)
        {
            TT2AUBridge bridge = new TT2AUBridge
            {
                Policy = policy
            };
            return bridge.ExportPolicyInfo();
        }

        private static void CalculateResponse(AutoResponseModel _autoResponseModel, List<ITCRateEngineResponse> _allITCRateResponses,
           List<ResponseCompanyDetails> _RespCompanyList)
        {
            _autoResponseModel.Response = new List<AutoRateCompanyResponse>();
            var _allITCDistict = _allITCRateResponses.Select(s => s.CompanyID).Distinct().ToList();
            foreach (var _itcCompanyId in _allITCDistict)
            {
                AutoRateCompanyResponse _autoRateCompanyResponse = new AutoRateCompanyResponse
                {
                    AutoRateResults = new List<AutoRateResult>()
                };
                try
                {
                    var _companyListByCompanyId = _allITCRateResponses.Where(w => w.CompanyID == _itcCompanyId).ToList();
                    foreach (var _itcresponse in _companyListByCompanyId)
                    {
                        //Vehicle

                        if (_itcresponse.Errors.Count() > 0)
                        {
                            List<ErrorResponse> errorResponse = new List<ErrorResponse>();
                            ErrorResponse _err = new ErrorResponse();
                            _err.Message = _itcresponse.Errors[0].Text;
                            _err.Amount = (int)_itcresponse.Errors[0].Amount;
                            _err.Code = _itcresponse.Errors[0].Code.ToString();
                            errorResponse.Add(_err);

                            _autoRateCompanyResponse.Errors = errorResponse;
                        }

                        //Vehicle
                        _autoRateCompanyResponse.Vehicles = new List<AutoResponseVehicle>();
                        foreach (var _autoVehicle in _itcresponse.Cars)
                        {
                            AutoResponseVehicle _autoResponseVehicle = new AutoResponseVehicle
                            {
                                Id = _autoVehicle.CarNumber,
                                Model = _autoVehicle.Model,
                                Make = _autoVehicle.Make,
                                VIN = _autoVehicle.VIN,
                                Year = _autoVehicle.Year
                            };
                            _autoResponseVehicle.Coverages = new List<AutoResponseCoverage>();
                            foreach (var _autoVehicleCoverage in _autoVehicle.Coverages)
                            {
                                AutoResponseCoverage _autoResponseCoverage = new AutoResponseCoverage
                                {
                                    CoverageName = _autoVehicleCoverage.CoverageName,
                                    Deductible = _autoVehicleCoverage.CoverageName,
                                    Premium = Convert.ToString(_autoVehicleCoverage.Premium),
                                    Limit = _autoVehicleCoverage.Limit.Select(s => Convert.ToString(s)).ToList()
                                };
                                _autoResponseVehicle.Coverages.Add(_autoResponseCoverage);
                            }
                            _autoRateCompanyResponse.Vehicles.Add(_autoResponseVehicle);
                        }

                        //Driver
                        _autoRateCompanyResponse.Drivers = new List<AutoResponseDriver>();
                        foreach (var _autoDriverResp in _itcresponse.Drivers)
                        {
                            AutoResponseDriver _autoResponseDriver = new AutoResponseDriver
                            {
                                Id = _autoDriverResp.DriverNumber,
                                Age = _autoDriverResp.Age,
                                AssignVehicleId = _autoDriverResp.AssignedCar,
                                Class = _autoDriverResp.DriverClass,
                                CreditScore = _autoDriverResp.CreditScore,
                                Marital = _autoDriverResp.Marital,
                                Relation = _autoDriverResp.Relation,
                                Sex = _autoDriverResp.Sex,
                                TicketPoints = _autoDriverResp.Points
                            };
                            _autoRateCompanyResponse.Drivers.Add(_autoResponseDriver);
                        }

                        _autoRateCompanyResponse.TransactionId = _itcresponse.TransactionID;
                        _autoRateCompanyResponse.CustomerId = _itcresponse.CustomerID;
                        _autoRateCompanyResponse.PhoneCode = _itcresponse.PhoneCode;
                        _autoRateCompanyResponse.BridgeUrl = _itcresponse.BuyNowUrl;
                        _autoRateCompanyResponse.LogoUrl = _itcresponse.LogoUrl;
                        _autoRateCompanyResponse.AgencyId = _itcresponse.AgencyID.ToString();
                        _autoRateCompanyResponse.AgencyName = _itcresponse.AgencyName;
                        _autoRateCompanyResponse.AgencyAddress1 = _itcresponse.AgencyAddress1;
                        _autoRateCompanyResponse.AgencyAddress2 = _itcresponse.AgencyAddress2;
                        _autoRateCompanyResponse.AgencyCity = _itcresponse.AgencyCity;

                        //query
                        _autoRateCompanyResponse.AgencyStateCd = _itcresponse.AgencyState.ToString();

                        _autoRateCompanyResponse.AgencyZipcode = _itcresponse.AgencyZip;
                        _autoRateCompanyResponse.AgencyPhone = _itcresponse.AgencyPhone;
                        _autoRateCompanyResponse.AgencyMarketingLine = _itcresponse.AgencyMarketingLine;
                        _autoRateCompanyResponse.CompanyId = _itcresponse.CompanyID.ToString();
                        _autoRateCompanyResponse.CompanyName = _RespCompanyList.Where(w => w.CompanyId == _itcresponse.CompanyID).Select(s => s.CompanyName).FirstOrDefault();
                        //_autoRateCompanyResponse.CompanyName = _itcresponse.CompanyName;
                        _autoRateCompanyResponse.Term = _itcresponse.Term;
                        _autoRateCompanyResponse.EffectiveDate = _itcresponse.EffectiveDate;
                        _autoRateCompanyResponse.ExpirationDate = _itcresponse.ExpirationDate;
                        _autoRateCompanyResponse.BridgeUrl = _itcresponse.RTRThirdPartyQuoteURL;

                        AutoRateResult autoRateResult = new AutoRateResult
                        {
                            AgencyFee = _itcresponse.AgencyFee,
                            AllRatingComplete = _itcresponse.AllRatingComplete,
                            DownPayment = _itcresponse.PayPlanDownPayment,
                            InstallmentFee = 0,
                            MontlyPayment = _itcresponse.PayPlanPaymentAmount,
                            NumOfPayments = _itcresponse.PayPlanNumOfPayments,
                            PayPlanDescription = _itcresponse.PayPlanDescription,
                            PayPlanId = _itcresponse.ProgramName,
                            PercentDownPayment = _itcresponse.PayPlanPercentDown,
                            PolicyFee = _itcresponse.PolicyFee,
                            ServiceFees = _itcresponse.PayPlanServiceFees,
                            TotalPremium = _itcresponse.TotalPremium
                        };

                        //autoRateResult.AutoInstallmentInfos
                        //AutoInstallmentInfos pending
                        _autoRateCompanyResponse.AutoRateResults.Add(autoRateResult);

                        //Discount & Surcharge
                        _autoRateCompanyResponse.DiscountsAndSurcharges = new List<DiscountsAndSurcharge>();
                        foreach (var _autoDisSurcharge in _itcresponse.DiscountsAndSurcharges)
                        {
                            DiscountsAndSurcharge _disSurchargeModel = new DiscountsAndSurcharge
                            {
                                Amount = _autoDisSurcharge.Amount,
                                Code = Convert.ToString(_autoDisSurcharge.Code),
                                Description = _autoDisSurcharge.Text,
                                Percentage = _autoDisSurcharge.Percentage
                            };
                            _autoRateCompanyResponse.DiscountsAndSurcharges.Add(_disSurchargeModel);
                            //query
                            //_disSurchargeModel.Type = _autoDisSurcharge.Scope;
                        }
                    }
                }
                catch (Exception ex)
                {

                    List<ErrorResponse> errorResponse = new List<ErrorResponse>();
                    ErrorResponse _err = new ErrorResponse();
                    _err.Message = ex.Message;
                    _err.Amount = 0;
                    _err.Code = Convert.ToString(SysEnums.SIError.ITCSYSTEMERROR);
                    errorResponse.Add(_err);

                    _autoRateCompanyResponse.Errors = errorResponse;
                }
                _autoResponseModel.Response.Add(_autoRateCompanyResponse);
            }
        }
    }
}
