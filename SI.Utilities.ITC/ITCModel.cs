﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SI.Utilities.ITC
{

    public class ITCModel
    {
        
    }

    //Retrieves a list of supported model years.
    public class Years
    {
        public int Year { get; set; }
    }


    //Retrieves a list of vehicle makes for a specified model year.
    public class VehicleList
    {
        public string Vehicle { get; set; }
    }

    //Retrieves a list of vehicle models for a specified year and make.
    public class VehicleModel
    {
        public string VehicleModelName { get; set; }
    }

    //Retrieves a list of vehicle detail objects for a specific year, make and model of vehicle.
    public class VehicleDetailModel
    {
        public string AntiLock { get; set; }
        public string BodyType { get; set; }
        public string FuelType { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public int ModelGroupCode { get; set; }
        public int ModelCode { get; set; }
        public string PassSeatRestraint { get; set; }
        public string AirBags { get; set; }
        public string AntiTheft { get; set; }
        public string TruckSize { get; set; }
        public int UniqueSymCode { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public bool FrontWD { get; set; }
        public int MSRP { get; set; }
        public int NumOfCyl { get; set; }
        public int NumOfDoors { get; set; }
        public string VehicleType { get; set; }
    }

}
