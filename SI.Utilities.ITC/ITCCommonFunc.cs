﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurboRater;

namespace SI.Utilities.ITC
{
    public class ITCCommonFunc
    {
        public static USState GetITCStateValue(string stateCode)
        {
            USState usstate = new USState();
            switch (stateCode)
            {
                case "AL":
                    return USState.Alabama;
                case "AK":
                    return USState.Alaska;
                case "AZ":
                    return USState.Arizona;
                case "AR":
                    return USState.Arkansas;
                case "CA":
                    return USState.California;
                case "CO":
                    return USState.Colorado;
                case "CT":
                    return USState.Connecticut;
                case "DE":
                    return USState.Delaware;
                case "FL":
                    return USState.Florida;
                case "GA":
                    return USState.Georgia;
                case "HI":
                    return USState.Hawaii;
                case "ID":
                    return USState.Idaho;
                case "IL":
                    return USState.Illinois;
                case "IN":
                    return USState.Indiana;
                case "IA":
                    return USState.Iowa;
                case "KS":
                    return USState.Kansas;
                case "KY":
                    return USState.Kentucky;
                case "LA":
                    return USState.Louisiana;
                case "ME":
                    return USState.Maine;
                case "MD":
                    return USState.Maryland;
                case "MA":
                    return USState.Massachusetts;
                case "MI":
                    return USState.Michigan;
                case "MN":
                    return USState.Minnesota;
                case "MS":
                    return USState.Mississippi;
                case "MO":
                    return USState.Missouri;
                case "MT":
                    return USState.Montana;
                case "NE":
                    return USState.Nebraska;
                case "NV":
                    return USState.Nevada;
                case "NH":
                    return USState.NewHampshire;
                case "NJ":
                    return USState.NewJersey;
                case "NM":
                    return USState.NewMexico;
                case "NY":
                    return USState.NewYork;
                case "NC":
                    return USState.NorthCarolina;
                case "ND":
                    return USState.NorthDakota;
                case "OH":
                    return USState.Ohio;
                case "OK":
                    return USState.Oklahoma;
                case "OR":
                    return USState.Oregon;
                case "PA":
                    return USState.Pennsylvania;
                case "RI":
                    return USState.RhodeIsland;
                case "SC":
                    return USState.SouthCarolina;
                case "SD":
                    return USState.SouthDakota;
                case "TN":
                    return USState.Tennessee;
                case "TX":
                    return USState.Texas;
                case "UT":
                    return USState.Utah;
                case "VT":
                    return USState.Vermont;
                case "VA":
                    return USState.Virginia;
                case "WA":
                    return USState.Washington;
                case "WV":
                    return USState.WestVirginia;
                case "WI":
                    return USState.Wisconsin;
                case "WY":
                    return USState.Wyoming;
            }

            return usstate;
        }

        public static int ITCCoverageConverter(int value)
        {
            return (value / 1000);
        }
    }
}
