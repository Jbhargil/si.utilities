﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SI.Utilities.Model
{
   public class PaymentResponse
    {
        public long QuoteID { get; set; }
        public long? ReceiptID { get; set; }
        public string ResponseText { get; set; }
        public string Amount { get; set; }
        public string ReturnRef { get; set; }
        public string ResponseCode { get; set; }
        public string Account { get; set; }
        public string MerchentID { get; set; }
        public string Token { get; set; }
        public string TempToken { get; set; }
        public string BatchID { get; set; }
        public string DefaultAccount { get; set; }
        public string IsSuccess { get; set; }
    }
    
}
