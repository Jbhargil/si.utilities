﻿using System;
using System.Collections.Generic;

namespace SI.Utilities.Model
{
    public class AutoRequestModel
    {
        public int Api_Version { get; set; }
        public string Api_Key { get; set; }
        public AutoPolicy AutoPolicy { get; set; }
        public List<AutoDriver> AutoDrivers { get; set; }
        public List<AutoVehicle> AutoVehicles { get; set; }
        public AutoCoverage Coverage { get; set; }
    }
    public class AutoPolicy
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ContactNo { get; set; }
        public DateTime? BirthDate { get; set; }
        public string OtherContactNo { get; set; }
        public string EmailAddress { get; set; }
        public string Street_Address1 { get; set; }
        public string Street_Address2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string StateCd { get; set; }
        public string County { get; set; }
        public string MailingStreet_Address1 { get; set; }
        public string MailingStreet_Address2 { get; set; }
        public string MailingZipCode { get; set; }
        public string MailingCity { get; set; }
        public string MailingStateCd { get; set; }
        public string MailingCounty { get; set; }
        public int PolicyTerm { get; set; }
        public bool Currently_Insured { get; set; }
        public string Current_CarrierName { get; set; }
        public int Current_Insured_Duration { get; set; }
        public DateTime EffectiveDate { get; set; } = DateTime.Today;
        public Dictionary<string, string> Coverage_Level { get; set; }
    }

    public class AutoDriver
    {
        public int Id { get; set; }
        public string License_StateCd { get; set; }
        public string License_Number { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ContactNo { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool HomeOwner { get; set; }
        public string Gender { get; set; }
        public string Marital_Status { get; set; }
        public bool Education_Completed { get; set; }
        public int Age_First_Licensed { get; set; }
        public bool IsSR22 { get; set; }
        public string SR22CaseNumber { get; set; }
        public string SR22Reason { get; set; }
        public string SR22State { get; set; }
        public string SSN { get; set; }
        public bool Is_Same_Address_As_Primary { get; set; }
        public bool IsExcluded { get; set; }
        public string Occupation { get; set; }
        public string RelationCd { get; set; }
        public bool IsGoodCredit { get; set; }
        public bool IsGoodStudent { get; set; }
        public AutoPriorInsurer PriorInsurer { get; set; }
        public int? PrimaryVehicleId { get; set; }

        public List<AutoDriverViolation> AutoDriverViolations { get; set; }
    }

    public class AutoDriverViolation
    {
        public int DriverID { get; set; }
        public string VioCode1 { get; set; }
        public string VioCode2 { get; set; }
        public DateTime VioDate { get; set; }
        public string Description { get; set; }
        public bool IsMVRAppeal { get; set; }
        public bool AtFault { get; set; } = false;
        public string Location { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZipCode { get; set; }
    }
    public class AutoPriorInsurer
    {
        public string InsurerName { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int Duration { get; set; }
        public string IsPreviouslyInsured { get; set; }
    }

    public class AutoVehicle
    {
        public int Id { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int AnnualMileage { get; set; }
        public bool IsOwned { get; set; }
        public string PrimarUse { get; set; }
        public string ComprehensiveDed { get; set; }
        public string CollisionDed { get; set; }
        public string TowingLimit { get; set; }
        public string RentalLimit { get; set; }
        public decimal? CarPurchaseCost { get; set; }
        public AutoLienHolder AutoLienHolder { get; set; }
    }

    public class AutoLienHolder
    {
        public string Name { get; set; }
        public string Street_Address1 { get; set; }
        public string Street_Address2 { get; set; }
        public string City { get; set; }
        public string StateCd { get; set; }
        public string ZipCode { get; set; }
    }

    public class AutoCoverage
    {
        //BI 
        public string BodilyInjuryPerPerson { get; set; }
        public string BodilyInjuryPerAcc { get; set; }

        //PD
        public string PropertyDamage { get; set; }

        //MED
        public string MedicalPayments { get; set; }

        //UMBI
        public string UninsuredBodilyInjuryPerPerson { get; set; }
        public string UninsuredBodilyInjuryPerAcc { get; set; }
        public string UninsuredBodilyInjuryDeductible { get; set; }

        //UMPD
        public string UninsuredPropertyDamage { get; set; }
        public string UninsuredPropertyDamageDeductible { get; set; }

        //UNBI
        public string UnderinsuredBodilyInjuryPerPerson { get; set; }
        public string UnderinsuredBodilyInjuryPerAcc { get; set; }
        public string UnderinsuredPropertyDamage { get; set; }

        //PIP
        public string PersonalInjuryProtection { get; set; }
        public string PersonalInjuryProtectionDed { get; set; }
        public string AdditionalersonalInjuryProtection { get; set; }

        //PPI
        public string PropertyProtectionInsurance { get; set; }

        //AM
        public string AnnualMileage { get; set; }

        //Tort
        public string Tort { get; set; }
    }
}
