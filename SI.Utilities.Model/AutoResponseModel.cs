﻿using System;
using System.Collections.Generic;

namespace SI.Utilities.Model
{
    public class AutoResponseModel
    {
        public List<AutoRateCompanyResponse> Response { get; set; }
        public string ResponseFile { get; set; }
    }

    public class AutoRateCompanyResponse
    {

        public List<AutoResponseVehicle> Vehicles { get; set; }

        public List<AutoResponseDriver> Drivers { get; set; }

        //Customer Details
        public string TransactionId { get; set; }
        public string CustomerId { get; set; }
        public string PhoneCode { get; set; }
        public string BridgeUrl { get; set; }
        public string LogoUrl { get; set; }

        //Agency Details
        public string AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyAddress1 { get; set; }
        public string AgencyAddress2 { get; set; }
        public string AgencyCity { get; set; }
        public string AgencyStateCd { get; set; }
        public string AgencyZipcode { get; set; }
        public string AgencyPhone { get; set; }
        public string AgencyMarketingLine { get; set; }

        //Rater Company Details
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyQuoteId { get; set; }
        //Policy Term
        public int Term { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }

        //Policy Rates
        public List<AutoRateResult> AutoRateResults { get; set; }

        //Discount Details
        public List<DiscountsAndSurcharge> DiscountsAndSurcharges { get; set; }

        //Error Details
        public List<ErrorResponse> Warnings { get; set; }
        public List<ErrorResponse> Errors { get; set; }

        public Dictionary<string,string> PolicyMetaDescription { get; set; }

    }

    public class AutoRateResult
    {
        public double PolicyFee { get; set; }
        public double AgencyFee { get; set; }
        public double TotalPremium { get; set; }
        public bool AllRatingComplete { get; set; } = true;
        public string PayPlanId { get; set; }
        public string PayPlanDescription { get; set; }
        public int NumOfPayments { get; set; }
        public double PercentDownPayment { get; set; }
        public double DownPayment { get; set; }
        public double MontlyPayment { get; set; }
        public double InstallmentFee { get; set; }
        public double ServiceFees { get; set; }

        public List<AutoInstallmentInfo> AutoInstallmentInfos { get; set; }
    }

    public class AutoResponseVehicle
    {
        public int Id { get; set; }
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public List<AutoResponseCoverage> Coverages { get; set; }
    }

    public class AutoInstallmentInfo
    {
        public int Number { get; set; }
        public double Amount { get; set; }
        public DateTime DueDate { get; set; }
    }

    public class AutoResponseCoverage
    {
        public string CoverageCd { get; set; }
        public string CoverageName { get; set; } 
        public List<string> Limit { get; set; }
        public string Deductible { get; set; }
        public string Premium { get; set; }
    }

    public class AutoResponseDriver
    {
        public int Id { get; set; }
        public double TicketPoints { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public string Marital { get; set; }
        public string Relation { get; set; }
        public string Class { get; set; }
        public string CreditScore { get; set; }
        public int AssignVehicleId { get; set; }
    }

    public class DiscountsAndSurcharge
    {
        public double Amount { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public string Code { get; set; }
        public SysEnums.AutoDisSurType Type { get; set; }
    }

    public class ErrorResponse
    {
        public int Amount { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
    }
    public class ResponseCompanyDetails
    {
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}
