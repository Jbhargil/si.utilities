﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SI.Utilities.Model
{
    public class SysEnums
    {
        public enum AutoDisSurType
        {
            DISCOUNT = 1,
            SURCHARGE = 2
        }

        public enum MaritalStatus
        {
            DISCOUNT = 1,
            SURCHARGE = 2
        }

        public enum SIError
        {
            ITCSYSTEMERROR
        }
    }
}
