﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SI.Utilities.Model
{
    public class PaymentRequest
    {
        public long QuoteID { get; set; }
        public long? ReceiptID { get; set; }
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CVV { get; set; }
        public string Account { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Addres2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string TempToken { get; set; }
        public PaymentGatewayCredential paymentGatewayCredential { get; set; }
        public PaymentDetail PaymentDetail { get; set; }
    }
    public class PaymentGatewayCredential
    {
        public string UserName { get; set; }
        public string EndPointURL { get; set; }
        public string Password { get; set; }
        public string MerchantID { get; set; }
    }
    public class PaymentDetail
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
